ARG FROM_IMAGE
FROM $FROM_IMAGE

# taken from https://github.com/docker-library/docker/blob/89b91f8164bd6a6bce035a1b8915df566994fb84/23.0/dind/Dockerfile
# https://github.com/docker/docker/blob/master/project/PACKAGERS.md#runtime-dependencies
RUN set -eux; \
    apk add --no-cache \
        btrfs-progs \
        e2fsprogs \
        e2fsprogs-extra \
        ip6tables \
        iptables \
        openssl \
        shadow-uidmap \
        xfsprogs \
        xz \
# pigz: https://github.com/moby/moby/pull/35697 (faster gzip implementation)
        pigz \
    ; \
# only install zfs if it's available for the current architecture
# https://git.alpinelinux.org/cgit/aports/tree/main/zfs/APKBUILD?h=3.6-stable#n9 ("all !armhf !ppc64le" as of 2017-11-01)
# "apk info XYZ" exits with a zero exit code but no output when the package exists but not for this arch
    if zfs="$(apk info --no-cache --quiet zfs)" && [ -n "$zfs" ]; then \
        apk add --no-cache zfs; \
    fi

# TODO aufs-tools

# set up subuid/subgid so that "--userns-remap=default" works out-of-the-box
RUN set -eux; \
    addgroup -S dockremap; \
    adduser -S -G dockremap dockremap; \
    echo 'dockremap:165536:65536' >> /etc/subuid; \
    echo 'dockremap:165536:65536' >> /etc/subgid
# END TAKEN

RUN apk add --no-cache docker

RUN wget -O /usr/local/bin/dind "https://raw.githubusercontent.com/moby/moby/$(dockerd --version | grep -owE [0-9a-f]{40})/hack/dind"; \
    chmod +x /usr/local/bin/dind

COPY entrypoint.sh /usr/local/bin/dockerd-entrypoint.sh

VOLUME /var/lib/docker
EXPOSE 2375 2376
ENTRYPOINT ["dockerd-entrypoint.sh"]
CMD []
